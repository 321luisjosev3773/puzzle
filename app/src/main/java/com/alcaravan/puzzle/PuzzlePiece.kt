package com.alcaravan.puzzle

import android.content.Context
import androidx.appcompat.widget.AppCompatImageView

class PuzzlePiece(context: Context?):AppCompatImageView(context!!) {

    var xCoord: Int = 0
    var yCoord: Int = 0
    var pieceWidth: Int = 0
    var pieceHeight: Int = 0
    var canMove: Boolean = true

}