package com.alcaravan.puzzle

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.GridView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import android.Manifest;
import android.app.Activity
import android.app.AlertDialog
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    var mCurrentPhotoPath: String? = null
    private val REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 2
    private val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_PERMISSION_READ_EXTERNAL_STORAGE: Int = 3
    val REQUEST_IMAGE_GALLERY: Int = 4

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        val am = assets
        try {
            val files = am.list("img")
            val grid = findViewById<GridView>(R.id.grid)
            grid.adapter = ImageAdapter(this)
            grid.onItemClickListener =
                OnItemClickListener { adapterView, view, i, l ->
                    level("assetName",files!![i % files!!.size] )
                }
        } catch (e: IOException) {
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    fun level(keys: String, s: String?= null){
        findViewById<View>(R.id.main)?.let {
            val opciones = arrayOf("Fácil", "Normal", "Difícil")
            val builder = AlertDialog.Builder(this)
            builder.setTitle("¡Elige tu desafío!")
            builder.setItems(opciones) { _, which ->
                navigate(which, keys, s)
            }
            builder.show()


        }
    }
    fun navigate(level: Int, keys: String, s: String?) {
        val intent = Intent(
            this,
            PuzzleActivity::class.java
        )
        when(keys){
            "assetName" -> intent.putExtra(keys,s)
            "mCurrentPhotoPath" -> intent.putExtra(keys,mCurrentPhotoPath)
            "mCurrentPhotoUri" -> intent.putExtra(keys,s)
        }
        intent.putExtra("level", level)
        startActivity(intent)
    }
    private val takePictureLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // Procesa la imagen capturada (photoUri contiene la URI de la imagen)
            // ...
            onImageFromCameraClick(View(this))
        }
    }
    fun requestPermissions() {
        val permissions = mutableListOf<String>()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA)
        }
        // Solo solicita WRITEs_EXTERNAL_STORAGE si es necesario (Android 12 o anterior)

        if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.S_V2 &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (permissions.isNotEmpty()) {
            ActivityCompat.requestPermissions(this, permissions.toTypedArray(), REQUEST_IMAGE_CAPTURE)
        } else {
            // Todos los permisos ya están concedidos
            // Inicia la cámara o realiza la acción deseada
            // Inicia la cámara (llama a la función startCamera())
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (e: IOException) {
                Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
            }

            if (photoFile != null) {
                val photoUri = FileProvider.getUriForFile(
                    this,
                    applicationContext.packageName + ".fileprovider",
                    photoFile
                )
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
            }
            else{

            }
        }
    }
    fun onImageFromCameraClick(view: View?) {
        requestPermissions()

    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
        mCurrentPhotoPath = image.absolutePath // save this to use in the intent

        return image
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (grantResults.isNotEmpty() && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                // Todos los permisos concedidos
                // Inicia la cámara o realiza la acción deseada
                onImageFromCameraClick(View(this))
            } else {
                // Al menos un permiso fue denegado
                // Muestra un mensaje al usuario o toma otra acción
                findViewById<View>(R.id.main)?.let {
                    Snackbar.make(it, "Se necesita permiso de cámara para usar esta función.", Snackbar.LENGTH_LONG)
                        .setAction("Configuración") {
                            // Abre la pantalla de configuración de permisos de la aplicación
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivity(intent)
                        }
                        .show()
                }
            }
        }
}

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            level("mCurrentPhotoPath")
        }
        if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
            val uri = data?.data

            level("mCurrentPhotoUri", uri.toString())
        }
    }
    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                // Permiso concedido, abre la galería
                openGallery()
            } else {
                // Permiso denegado, muestra un mensaje o toma otra acción
                // ...
                findViewById<View>(R.id.main)?.let {
                    Snackbar.make(it, "Se necesita permiso para acceder a la galería.", Snackbar.LENGTH_LONG)
                        .setAction("Configuración") {
                            // Abre la pantalla de configuración de permisos de la aplicación
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivity(intent)
                        }
                        .show()
                }
            }
        }

    private fun openGallery() {

        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("image/*")
        startActivityForResult(intent, REQUEST_IMAGE_GALLERY)
    }

    fun onImageFromGalleryClick(view: View?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            // Android 13+
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_MEDIA_IMAGES
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissionLauncher.launch(Manifest.permission.READ_MEDIA_IMAGES)
            } else {
                openGallery()
            }
        } else {
            // Android 12 o inferior (usa READ_EXTERNAL_STORAGE)
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_PERMISSION_READ_EXTERNAL_STORAGE
                )
            } else {
                openGallery()
            }
        }
    }
}